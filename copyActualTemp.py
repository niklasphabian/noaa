import energyDB.dbTable.climate
import datetime
import pytz
import database 
cet =  pytz.timezone('CET')

def getActual():  
    startTime = cet.localize(datetime.datetime(2010,1,1))
    endTime = cet.localize(datetime.datetime(2015,6,30))
    dbTab = energyDB.dbTable.climate.TemperatureTable()
    ret = dbTab.getTimeSeries(startTime, endTime, 4177)
    return ret

actualDB = database.Actual()

actual = getActual()
for row in actual:
    timestamp = row[0]
    temp = row[1]
    actualDB.insertRow(timestamp, temp)

actualDB.commit()
actualDB.closeCon()